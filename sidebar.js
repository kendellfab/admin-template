document.addEventListener("DOMContentLoaded", () => {
    const arrowRight = "arrow-right-circle.png";
    const arrowLeft = "arrow-left-circle.png";
    const isExpanded = "is-expanded";
    const hasAsideExpanded = "has-aside-expanded";
    const hasAsideMobileExpanded = "has-aside-mobile-expanded";

    const $html = document.getElementsByTagName("html")[0];
    const sidebar = document.getElementById("sidebar");
    const toggleSidebar = document.getElementById("toggleSidebar");
    const toggleIcon = document.getElementById("toggleIcon");

    toggleSidebar.addEventListener('click', () => {

        var windowWidth = window.innerWidth;
        console.log(windowWidth);

        if(windowWidth >= 1024) {

            if (sidebar.classList.contains(isExpanded)) {
                sidebar.classList.remove(isExpanded);
                $html.classList.remove(hasAsideExpanded);
                toggleIcon.src = arrowRight;
            } else {
                sidebar.classList.add(isExpanded);
                $html.classList.add(hasAsideExpanded);
                toggleIcon.src = arrowLeft;
            }
        } else {
            if($html.classList.contains(hasAsideMobileExpanded)) {
                $html.classList.remove(hasAsideMobileExpanded);
                toggleIcon.src = arrowRight;
            } else {
                $html.classList.add(hasAsideMobileExpanded);
                toggleIcon.src = arrowLeft;
            }
        }
    });

    window.addEventListener("resize", () => {
        updateWindow();
    });

    function updateWindow() {

    }
});